import WebSocket from 'ws'

export const wss = new WebSocket.Server({ port: 9000 })
console.log('hit')
wss.on('connection', (ws) => {
  ws.on('message', (message) => {
    console.log(`client said ${message}`)
  })

  ws.on('close', () => ws.close())
})
