'use client'

import styles from "./page.module.css";
import { useWebSocket } from "./hooks/useWebSockets";

export default () => {
  const [ws, _] = useWebSocket('ws://localhost:9000')
  console.log(ws)

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log('hit')
  }

  return (
    <main className={styles.main}>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="message">Your Message:</label>
          <input id="message" type="text" />
        </div>
        <button type="submit">Submit</button>
      </form>
    </main>
  );
}
