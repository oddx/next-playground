import { useCallback, useState } from "react";

export const useWebSocket = (url: string) => {
  const [wsInstance, setWsInstance] = useState<WebSocket | undefined>(undefined);

  const updateWebSocket = useCallback(() => {
    const browser = typeof window !== undefined

    if (!browser) return setWsInstance(undefined);

    if (wsInstance?.readyState !== 3) { wsInstance?.close() }

    setWsInstance(new WebSocket(url))
  }, [wsInstance])

  return [wsInstance, updateWebSocket]
}
